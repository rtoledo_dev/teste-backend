class AddKindOfToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :kind_of, :integer
  end
end
