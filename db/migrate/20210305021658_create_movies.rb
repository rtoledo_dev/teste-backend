class CreateMovies < ActiveRecord::Migration[6.1]
  def change
    create_table :movies do |t|
      t.references :user, null: false, foreign_key: true
      t.string :title
      t.text :summary
      t.string :director
      t.string :gender
      t.text :actors

      t.timestamps
    end
  end
end
