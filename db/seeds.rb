require 'faker'
User.create!(email: 'admin@iosys.com.br', password: 'password', password_confirmation: 'password', kind_of: :administrator)
User.create!(email: 'user@iosys.com.br', password: 'password', password_confirmation: 'password', kind_of: :user)

10.times do
  Movie.create!(
    user_id: User.last.id,
    title: Faker::Movie.title + "-" + Faker::Name.name,
    summary: Faker::Lorem.paragraph,
    director: Faker::Name.name,
    gender: "Action",
    actors: Faker::Name.name,
  )
end
