Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
  }
  namespace :api do
    resources :movies, only: [:index, :create, :show] do
      resources :votes, only: :create
    end
  end
end
