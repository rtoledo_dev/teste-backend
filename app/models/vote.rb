class Vote < ApplicationRecord
  belongs_to :movie
  belongs_to :user
  validates :vote, presence: true
end
