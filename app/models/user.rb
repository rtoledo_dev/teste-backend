class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
    :rememberable, :validatable

  scope :administrators, -> { where(kind_of: :administrator) }
  scope :users, -> { where(kind_of: :user) }
  enum kind_of: [:administrator, :user]
  has_many :movies
  has_many :votes
  validates :kind_of, presence: :true
end
