class Movie < ApplicationRecord
  belongs_to :user
  has_many :votes
  validates :title, :summary, :director, :gender, :actors, presence: true

  def average_votes
    votes.average(:vote).to_i
  end
end
