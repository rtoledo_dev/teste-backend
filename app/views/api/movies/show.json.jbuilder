json.movie do |json|
  json.partial! 'api/movies/movie', movie: @movie
end
