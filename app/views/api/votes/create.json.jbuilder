json.vote do |json|
  json.partial! 'api/votes/vote', vote: @vote
end
