# frozen_string_literal: true
class Users::RegistrationsController < Devise::RegistrationsController
  before_action :verify_jwt_token, only: :update
  skip_before_action :authenticate_scope!, only: :update

  def create
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      sign_up(resource_name, resource)

      render json: {user: resource, token: generate_token(resource)}, status: :created
    else
      render json: {error: resource.errors}, status: :unprocessable_entity
    end
  end

  def update
    if current_user.update_without_password(configure_account_update_params)
      render json: {user: current_user, token: generate_token(current_user)}, status: :ok
    else
      render json: {errors: current_user.errors}, status: :unprocessable_entity
    end
  end

  protected

    def sign_up_params
      params.require(:user).permit(:email, :password, :password_confirmation, :kind_of)
    end

    def configure_account_update_params
      params.require(:user).permit(:email, :password)
    end
end
