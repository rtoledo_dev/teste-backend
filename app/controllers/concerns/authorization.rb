require 'auth_token'
module Authorization
  def verify_jwt_token
    head :unauthorized if request.headers['Authorization'].nil? ||
        !AuthToken.valid?(request.headers['Authorization'].split(' ').last)
  end

  def generate_token(resource)
    AuthToken.issue_token({ user_id: resource.id })
  end

  def set_current_user
    user_id = AuthToken.find_user_id(request.headers['Authorization'])
    User.find(user_id)
  end

  def current_user
    @current_user ||= set_current_user
  end
end
