class Api::VotesController < ApplicationController
  before_action :verify_jwt_token
  def create
    @vote = current_user.votes.build(movie_id: params[:movie_id], vote: params[:vote])
    unless @vote.save
      render json: {errors: @vote.errors}, status: :unprocessable_entity
    end
  end
end
