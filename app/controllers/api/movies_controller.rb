class Api::MoviesController < ApplicationController
  before_action :verify_jwt_token
  def index
    @movies = Movie.ransack(movie_search_params).result(distinct: true)
  end

  def create
    unless current_user.administrator?
      head :unauthorized
    else
      @movie = current_user.movies.build(movie_params)
      unless @movie.save
        render json: {errors: @movie.errors}, status: :unprocessable_entity
      end
    end

  end

  def show
    @movie = Movie.find(params[:id])
  end

  protected
    def movie_params
      params.require(:movie).permit(:title, :summary, :director, :gender, :actors)
    end

    def movie_search_params
      return if params[:search].nil?
      search = params[:search]
      search[:m] = 'or'
      search
    end
end
