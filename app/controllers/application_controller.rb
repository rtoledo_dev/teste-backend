class ApplicationController < ActionController::API
  respond_to :json
  include Authorization
end
