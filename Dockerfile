FROM ruby:2.7.2
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs postgresql-client
RUN mkdir /usr/src/iosys
WORKDIR /usr/src/iosys
ADD . /usr/src/iosys
RUN gem install bundler -v 2.0.1
RUN bundle install
