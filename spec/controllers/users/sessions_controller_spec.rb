require 'rails_helper'

RSpec.describe Users::SessionsController, type: :request do
  describe "#administrators" do
    let(:user) { create(:user, :for_user) }
    context "#sign_in" do
      before do
        post user_session_path, params: { user: { email: user.email, password: 'password' } }
      end

      it 'returns 201' do
        expect(response.status).to eq(201)
      end

      it 'returns a token' do
        response_formatted = JSON.parse(response.body)
        expect(response_formatted["user"]).to eql(user.email)
        expect(response_formatted["token"]).not_to be_nil
      end
    end
  end
end
