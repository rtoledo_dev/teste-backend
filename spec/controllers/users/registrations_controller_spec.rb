require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :request do
  let(:admin_valid_attributes) { { user: attributes_for(:user, :for_administrator) } }
  let(:user_valid_attributes) { { user: attributes_for(:user, :for_user) } }

  describe "#administrators" do

    context "#sign_up" do
      before do
        post user_registration_path, params: admin_valid_attributes
      end

      it 'returns 201' do
        expect(response.status).to eq(201)
      end

      it 'returns a token' do
        response_formatted = JSON.parse(response.body)
        expect(response_formatted["user"]["id"]).not_to be_nil
        expect(response_formatted["user"]["kind_of"]).to eql("administrator")
        expect(response_formatted["token"]).not_to be_nil
      end
    end
  end

  describe "#users" do
    context "#sign_up" do
      before do
        post user_registration_path, params: user_valid_attributes
      end

      it 'returns 201' do
        expect(response.status).to eq(201)
      end

      it 'returns a token' do
        response_formatted = JSON.parse(response.body)
        expect(response_formatted["user"]["id"]).not_to be_nil
        expect(response_formatted["user"]["kind_of"]).to eql("user")
        expect(response_formatted["token"]).not_to be_nil
      end
    end
  end
end
