require 'rails_helper'

RSpec.describe Api::VotesController, type: :request do
  let!(:jwt_token) { AuthToken.issue_token({ user_id: create(:user, :for_user).id }) }
  let!(:headers) { { 'ACCEPT': 'application/json', 'Authorization': jwt_token } }
  describe "#vote creation" do
    context "#create" do
      let!(:movie) { create(:movie, user: create(:user, :for_user)) }
      it "create vote" do
        post api_movie_votes_path(movie.id), params: {vote: 1}, headers: headers
        expect(response).to be_successful
        expect(assigns(:vote).vote).to eql(1)
        expect(assigns(:vote)).to be_persisted
      end

      it "create vote with more comment" do
        post api_movie_votes_path(movie.id), params: {vote: 3}, headers: headers
        expect(response).to be_successful
        expect(assigns(:vote).vote).to eql(3)
        expect(assigns(:vote)).to be_persisted
      end
    end
  end
end
