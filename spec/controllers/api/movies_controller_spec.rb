require 'rails_helper'

RSpec.describe Api::MoviesController, type: :request do
  let(:movie_params) { { movie: attributes_for(:movie) } }
  let!(:jwt_token) { AuthToken.issue_token({ user_id: create(:user, :for_administrator).id }) }
  let!(:headers) { { 'ACCEPT': 'application/json', 'Authorization': jwt_token } }

  describe "#movie creation" do
    context "#create" do
      it "create movie" do
        post api_movies_path, params: movie_params, headers: headers
        expect(response).to be_successful
        expect(Movie.count).to eql(1)
        expect(assigns(:movie)).to be_persisted
      end
    end
  end

  describe "#movie detail" do
    context "#show" do
      let(:movie) { create(:movie) }
      before do
        create(:vote, movie: movie, vote: 1)
        create(:vote, movie: movie, vote: 4)
      end
      it "show movie" do
        get api_movie_path(movie), headers: headers
        expect(response).to be_successful
        expect(assigns(:movie)).to be_persisted
        expect(assigns(:movie).average_votes).to eql(2)
      end
    end
  end

  describe "#movies list" do
    context "#index" do
      context "#without filter" do
        before do
          user = create(:user, :for_administrator)
          create_list(:movie, 10, user: user )
        end
        it "list movies" do
          get api_movies_path, headers: headers
          expect(response).to be_successful
          expect(assigns(:movies).count).to eql(10)
        end
      end

      context "#filter" do
        before(:all) do
          user = create(:user, :for_administrator)
          create_list(:movie, 10, user: user)
          create(:movie,
            title: "---own title---",
            director: "---own director---",
            gender: "---own gender---",
            actors: "---own actor one and two---",
            user: user)
          create(:movie,
            title: "---own titles---",
            actors: "---own actors---",
            user: user)
        end
        it "list movies by title" do
          get api_movies_path(search: {title_cont_all: 'titl'}), headers: headers
          expect(assigns(:movies).count).to eql(2)
        end

        it "list movies by director" do
          get api_movies_path(search: {director_cont_all: 'direc'}), headers: headers
          expect(assigns(:movies).count).to eql(1)
        end

        it "list movies by gender" do
          get api_movies_path(search: {gender_cont_all: 'gender'}), headers: headers
          expect(assigns(:movies).count).to eql(1)
        end

        it "list movies by actors" do
          get api_movies_path(search: {actors_cont_all: 'actor one'}), headers: headers
          expect(assigns(:movies).count).to eql(1)
        end

        it "list movies by title or actors" do
          get api_movies_path(search: {title_cont_all: '---title dont exist---', actors_cont_all: 'actors'}), headers: headers
          expect(assigns(:movies).count).to eql(1)
        end
      end
    end
  end
end
