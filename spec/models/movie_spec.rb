require 'rails_helper'

RSpec.describe Movie, type: :model do
  describe 'associations' do
    it { should belong_to(:user).class_name('User') }
    it { should have_many(:votes).class_name('Vote') }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:summary) }
    it { should validate_presence_of(:gender) }
    it { should validate_presence_of(:actors) }
  end

  describe "average of votes" do
    subject { create(:movie) }
    context "when dont have votes" do
      it "#average eql 0" do
        expect(subject.average_votes).to eql(0)
      end
    end

    context "when have votes" do
      before do
        subject.votes.build(user: create(:user, :for_user, email: Faker::Internet.email), vote: 0)
        subject.votes.build(user: create(:user, :for_user, email: Faker::Internet.email), vote: 2)
        subject.votes.build(user: create(:user, :for_user, email: Faker::Internet.email), vote: 4)
        subject.votes.build(user: create(:user, :for_user, email: Faker::Internet.email), vote: 3)
        subject.votes.build(user: create(:user, :for_user, email: Faker::Internet.email), vote: 2)
        subject.save
      end

      it "#average eql 2" do
        expect(subject.average_votes).to eql(2)
      end
    end
  end
end
