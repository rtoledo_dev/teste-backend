require 'rails_helper'

RSpec.describe User, type: :model do
  describe "administrator" do
    subject { User.new(attributes_for(:user, :for_administrator)) }
    it "is valid with valid attributes for administrator" do
      expect(subject).to be_valid
    end

    it "is kind of administrator" do
      expect(subject.administrator?).to be_truthy
    end

    it '#administrators scope' do
      expect {
        subject.save
      }.to change(User.administrators, :count).by(1)
    end
  end

  describe "user" do
    subject { User.new(attributes_for(:user, :for_user)) }

    it "is valid with valid attributes for user" do
      expect(subject).to be_valid
    end

    it "is kind of user" do
      expect(subject.user?).to be_truthy
    end

    it '#users scope' do
      expect {
        subject.save
      }.to change(User.users, :count).by(1)
    end
  end
end
