FactoryBot.define do
  factory :movie do
    user { create(:user, :for_administrator) }
    title { Faker::Movie.title + "-" + Faker::Name.name }
    summary { Faker::Lorem.paragraph }
    director { Faker::Name.name }
    gender { "Action" }
    actors { Faker::Name.name }
  end
end
