FactoryBot.define do
  factory :user do
    password { 'password' }
    password_confirmation { 'password' }
    email { Faker::Internet.email }
    trait :for_administrator do
      kind_of { 'administrator' }
    end

    trait :for_user do
      kind_of { 'user' }
    end
  end
end
