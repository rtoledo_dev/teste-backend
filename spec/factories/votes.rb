FactoryBot.define do
  factory :vote do
    movie
    user { create(:user, :for_user) }
    vote { 0 }
  end
end
